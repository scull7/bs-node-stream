open Ava

let dirname = Belt.Option.getExn [%bs.node __dirname]

let uppercase () = Node_stream.Duplex.make (fun buf _enc cb ->
  buf
  |> Node.Buffer.toString
  |> String.uppercase
  |> (cb Js.Nullable.null)
)

let dash_between_words () = Node_stream.Duplex.make (fun buf _enc cb ->
  buf
  |> Node.Buffer.toString
  |> Js.String.split(" ")
  |> Js.Array.joinWith("-")
  |> (cb Js.Nullable.null)
)

let assert_string_is promise t this =
  promise
  |> Js.Promise.then_ (fun that ->
      let _ = Assert.is t this that
      in
      Js.Promise.resolve ()
  )
  |> Js.Promise.catch (fun e ->
      let _ = Test.fail ~message:(Js.String.make e) t
      in
      Js.Promise.resolve ()
  )
  |> Js.Promise.then_ (fun _ ->
    let _ = Test.finish t
    in
    Js.Promise.resolve ()
  )

let () =

Async.test ava "String to upper case" (fun t ->
  let _ = Test.plan t 1 in

  "foo foo"
  |. Node_stream.Readable.fromString
  |. Node_stream.Readable.pipe(uppercase ())
  |. Node_stream.Readable.toPromise
  |. assert_string_is t "FOO FOO"
  |. ignore
);

Async.test ava "Uppercase, dasherize" (fun t ->
  let _ = Test.plan t 1 in

  "This is a test"
  |. Node_stream.Readable.fromString
  |. Node_stream.Readable.pipe(uppercase ())
  |. Node_stream.Readable.pipe(dash_between_words ())
  |. Node_stream.Readable.toPromise
  |. assert_string_is t "THIS-IS-A-TEST"
  |. ignore
);

Async.test ava "Uppercase names in file" (fun t ->
  let _ = Test.plan t 1 in

  dirname
  |. Node.Path.resolve "./assets/names.txt" 
  |. (fun path -> `String path)
  |. Node_stream_file.Read.make
  |. Node_stream.Readable.pipe(uppercase ())
  |. Node_stream.Readable.toPromise
  |. assert_string_is t "ADAM DAKOTA DAN DANNY DON NATE PIOTR\n"
  |. ignore
);

Async.test ava "Should write a file" (fun t ->
  let _ = Test.plan t 1 in

  let expected = "This is a test" in
  let path = Node.Path.resolve dirname "/tmp/node-stream-write-test.txt" in
  let writer = Node_stream_file.Write.make (`String path) in

  expected
  |. Node.Buffer.fromString
  |. Node_stream.Readable.fromBuffer
  |. Node_stream.Readable.sink writer
  |. Node_stream.Terminal.on (`finish (fun () ->
      let actual = Node.Fs.readFileAsUtf8Sync path in
      let _ = Assert.is t expected actual in
      let _ = Test.finish t in
      ()
  ))
  |. ignore
)

