# bs-node-stream

BuckleScript types for NodeStreams.

## What needs to be done

* [ ] - Object stream support
* [ ] - Implement examples from RisingStack's [The Definitive Guide]
* [ ] - Implement examples from Cody A Ray's [Intro to Streams2]

[The Definitive Guide]: https://community.risingstack.com/the-definitive-guide-to-object-streams-in-node-js/
[Intro to Streams2]: http://codyaray.com/2013/04/intro-to-streams2-new-node-streams-part-1
