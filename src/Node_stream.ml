
module rec Readable : sig
  type 'a t

  type options = {
    finish: bool [@bs.as "end"];
  } [@@bs.deriving abstract]


  val on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `data of 'a -> unit
      | `finish of unit -> unit
      | `error of exn -> unit 
      | `readable of unit -> unit
      ]
    ) ->
    'a t

  val destroy : 'a t -> exn -> 'a t

  val is_paused : 'a t -> bool

  val pause : 'a t -> 'a t

  val pipe : 'a t -> ?options:options -> ('a, 'b) Duplex.t -> 'b Readable.t

  val sink : 'a t -> ?options:options -> 'a Writable.t -> 'a Terminal.t

  val fromBuffer : Node.Buffer.t -> Node.Buffer.t t

  val fromString : string -> string t

  val fromArray : 'a array -> 'a array t

  val fromPromise : 'a Js.Promise.t -> 'a t

  val toPromise : 'a t -> 'a Js.Promise.t

  val toArray : 'a array t -> 'a array Js.Promise.t

  val toBuffer : Node.Buffer.t t -> Node.Buffer.t Js.Promise.t

end = struct
  type 'a t

  include Node_stream_external.Make_readable(
    struct
      type 'a stream = 'a t
    end
  )

  type options = {
    finish: bool [@bs.as "end"];
  } [@@bs.deriving abstract]

  external on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `data of 'a -> unit
      | `finish of unit -> unit [@bs.as "end"]
      | `error of exn -> unit 
      | `readable of unit -> unit
      ] [@bs.string]
    ) ->
    'a t
    = "on"
    [@@bs.send]

  external destroy : 'a t -> exn -> 'a t = "" [@@bs.send]

  external is_paused : 'a t -> bool = "isPaused" [@@bs.send]

  external pause : 'a t -> 'a t = "" [@@bs.send]
  
  external pipe :
    'a t -> ('a, 'b) Duplex.t -> options Js.Nullable.t -> 'b Readable.t
    = "pipe"
    [@@bs.send]

  external sink :
    'a t -> 'a Writable.t -> options Js.Nullable.t -> 'a Terminal.t
    = "pipe"
    [@@bs.send]

  let pipe t ?options duplex =
    let opts = Js.Nullable.fromOption options
    in
    pipe t duplex opts

  let sink t ?options writable =
    let opts = Js.Nullable.fromOption options
    in
    sink t writable opts
end


and Writable : sig
  type 'a t

  val on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `drain of unit -> unit
      | `error of exn -> unit
      | `finish of unit -> unit
      ]
    ) ->
    'a t

  val cork : 'a t -> 'a t

  val uncork : 'a t -> 'a t

end = struct
  type 'a t

  external on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `drain of unit -> unit
      | `error of exn -> unit
      | `finish of unit -> unit
      ] [@bs.string]
    ) ->
    'a t
    = "on"
    [@@bs.send]

  external cork : 'a t -> 'a t = "" [@@bs.send]

  external uncork : 'a t -> 'a t = "" [@@bs.send]
end


and Duplex : sig
  type ('a, 'b) t

  type options = {
    finish: bool [@bs.as "end"];
  } [@@bs.deriving abstract]

  val to_readable : ('a, 'b) t -> 'a Readable.t

  val to_writeable : ('a, 'b) t -> 'b Writable.t

  val destroy : ('a, 'b) t -> exn -> ('a, 'b) t

  val is_paused : ('a, 'b) t -> bool

  val pause : ('a, 'b) t -> ('a, 'b) t

  val pipe : ('a, 'b) t -> ?options:options -> ('a, 'b) Duplex.t -> 'b Readable.t

  val sink : ('a, 'b) t -> ?options:options -> 'a Writable.t -> 'a Terminal.t

  val cork : ('a, 'b) t -> ('a, 'b) t

  val uncork : ('a, 'b) t -> ('a, 'b) t

  type 'b cb = (exn Js.Nullable.t -> 'b -> unit)

  val make : (Node.Buffer.t -> string -> 'b cb -> unit) -> ('a, 'b) t

  val toPromise : ('a, 'b) t -> 'b Js.Promise.t

  val toArray : ('a, 'b array) t -> 'b array Js.Promise.t

  val toBuffer : ('a, Node.Buffer.t) t -> Node.Buffer.t Js.Promise.t

end = struct
  type ('a, 'b) t

  include Node_stream_external.Make_duplex(
    struct
      type ('a, 'b) stream = ('a, 'b) t
    end
  )

  type options = {
    finish: bool [@bs.as "end"];
  } [@@bs.deriving abstract]

  external to_readable : ('a, 'b) t -> 'a Readable.t = "%identity"

  external to_writeable : ('a, 'b) t -> 'b Writable.t = "%identity"

  external destroy : ('a, 'b) t -> exn -> ('a, 'b) t = "" [@@bs.send]

  external is_paused : ('a, 'b) t -> bool = "isPaused" [@@bs.send]

  external pause : ('a, 'b) t -> ('a, 'b) t = "" [@@bs.send]

  external cork : ('a, 'b) t -> ('a, 'b) t = "" [@@bs.send]

  external uncork : ('a, 'b) t -> ('a, 'b) t = "" [@@bs.send]
  
  external pipe :
    ('a, 'b) t -> ('a, 'b) Duplex.t -> options Js.Nullable.t -> 'b Readable.t
    = "pipe"
    [@@bs.send]

  external sink :
    ('a, 'b) t -> 'a Writable.t -> options Js.Nullable.t -> 'a Terminal.t
    = "pipe"
    [@@bs.send]

  let pipe t ?options duplex =
    let opts = Js.Nullable.fromOption options
    in
    pipe t duplex opts

  let sink t ?options writable =
    let opts = Js.Nullable.fromOption options
    in
    sink t writable opts
end


and Terminal : sig
  type 'a t

  val on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `finish of unit -> unit
      | `error of exn -> unit
      ]
    ) ->
    'a t
end = struct
  type 'a t

  external on :
    'a t ->
    (
      [
      | `close of unit -> unit
      | `finish of unit -> unit
      | `error of exn -> unit
      ] [@bs.string]
    ) ->
    'a t
    = "on"
    [@@bs.send]
end
