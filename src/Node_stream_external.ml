
module Make_readable(Impl: sig type 'a stream end) = struct

  external fromBuffer : Node.Buffer.t -> Node.Buffer.t Impl.stream
  = "into-stream"
  [@@bs.module]

  external fromString : string -> string Impl.stream
  = "into-stream"
  [@@bs.module]

  external fromArray : 'a array -> 'a array Impl.stream
  = "into-stream"
  [@@bs.module]

  external fromPromise : 'a Js.Promise.t -> 'a Impl.stream
  = "into-stream"
  [@@bs.module]

  external toPromise : 'a Impl.stream -> 'a Js.Promise.t
  = "get-stream" [@@bs.module]

  external toArray : 'a array Impl.stream -> 'a array Js.Promise.t
  = "array"
  [@@bs.module "get-stream"]

  external toBuffer : Node.Buffer.t Impl.stream -> Node.Buffer.t Js.Promise.t
  = "buffer"
  [@@bs.module "get-stream"]
end

module Make_duplex(Impl: sig type ('a, 'b) stream end) = struct

  type 'b cb = (exn Js.Nullable.t -> 'b -> unit)

  external make :
    (Node.Buffer.t-> string -> 'b cb -> unit)
    -> ('a, 'b) Impl.stream
    = "through2"
  [@@bs.module]

  external toPromise : ('a, 'b) Impl.stream -> 'b Js.Promise.t
    = "get-stream"
  [@@bs.module]

  external toArray : ('a, 'b array) Impl.stream -> 'b array Js.Promise.t
    = "array"
  [@@bs.module "get-stream"]

  external toBuffer :
    ('a, Node.Buffer.t) Impl.stream ->
    Node.Buffer.t Js.Promise.t
    = "buffer"
  [@@bs.module "get-stream"]
end
