
module Read = struct

  module Options = struct
    type t = {
      flags: string [@bs.optional];
      encoding: string [@bs.optional];
      fd: int [@bs.optional];
      mode: int [@bs.optional];
      auto_close: bool [@bs.as "autoClose"][@bs.optional];
      start: int [@bs.optional];
      finish: int [@bs.as "end"][@bs.optional];
      high_water_mark: int [@bs.as "highWaterMark"][@bs.optional];
    }[@@bs.deriving abstract]

    let make = t
  end

  external make :
    ([ | `String of string | `Buffer of Node.Buffer.t ][@bs.unwrap]) ->
    Node.Buffer.t Node_stream.Readable.t
    = "createReadStream"
  [@@bs.module "fs"]


  external make_with_options :
    ([ | `String of string | `Buffer of Node.Buffer.t ][@bs.unwrap]) ->
    Options.t Js.Nullable.t ->
    Node.Buffer.t Node_stream.Readable.t
    = "createReadStream"
  [@@bs.module "fs"]
end

module Write = struct
  
  module Options = struct
    type t = {
      flags: string [@bs.optional];
      encoding: string [@bs.optional];
      fd: int [@bs.optional];
      mode: int [@bs.optional];
      auto_close: bool [@bs.as "autoClose"][@bs.optional];
      start: int [@bs.optional];
    } [@@bs.deriving abstract]

    let make = t
  end

    external make :
      ([ | `String of string | `Buffer of Node.Buffer.t ][@bs.unwrap]) ->
      Node.Buffer.t Node_stream.Writable.t
      = "createWriteStream"
    [@@bs.module "fs"]


    external make_with_options :
      ([ | `String of string | `Buffer of Node.Buffer.t ][@bs.unwrap]) ->
      Options.t ->
      Node.Buffer.t Node_stream.Writable.t
      = "createWriteStream"
    [@@bs.module "fs"]
end
